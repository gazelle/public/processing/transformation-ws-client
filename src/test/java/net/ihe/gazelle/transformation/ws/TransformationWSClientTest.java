package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.TransformationException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Integration testing of the client with Gazelle Transformation Service.
 * The application Gazelle Transformation must be deployed with dfdl schemas mms_mmxu.dfd.xsd and rsp580.xsd deployed.
 * Remove the @Ignore to launch the tests.
 *
 * @author ceoche
 */
@Ignore
public class TransformationWSClientTest {

    private static final String TEST_END_POINT = "http://localhost:8580/transformation-ejb/GazelleTransformationService/Transformation?wsdl";

    private static final String RESOURCES_DIRECTORY = "src/test/resources";
    private static final String TW_DATA_PATH = RESOURCES_DIRECTORY + "/twlab/payload_157.asn1";
    private static final String TW_XML_PATH = RESOURCES_DIRECTORY + "/twlab/payloadTransformed.xml";
    private static final String RSP_DATA_PATH = RESOURCES_DIRECTORY + "/rsp580/valid_cs.dat";
    private static final String RSP_XML_PATH = RESOURCES_DIRECTORY + "/rsp580/valid_cs.xml";

    @Test
    public void transformDataGivenReferenceToDfdlSchemaTWTest()
            throws IOException, TransformationException, CompilationException, ObjectNotFoundException {

        byte[] data = getFileContent(TW_DATA_PATH);
        byte[] xml = getFileContent(TW_XML_PATH);

        TransformationWSClient transformationWSClient = new TransformationWSClient(TEST_END_POINT);
        TransformedXMLStringWithDFDLReference xmlResponse = transformationWSClient.transformDataGivenReferenceToDfdlSchema("MMS_MMXU", data);
        Assert.assertTrue("xmlResponse should not be null or empty",
                xmlResponse != null && xmlResponse.getTransformedXmlAsString() != null && !xmlResponse.getTransformedXmlAsString().isEmpty());

        byte[] transformedXml = xmlResponse.getTransformedXml();
        Assert.assertArrayEquals("Transformed data must be equals to expected", xml, transformedXml);

    }

    @Test
    public void transformDataGivenReferenceToDfdlSchemaRSPTest()
            throws IOException, TransformationException, CompilationException, ObjectNotFoundException {

        byte[] data = getFileContent(RSP_DATA_PATH);
        byte[] xml = getFileContent(RSP_XML_PATH);

        TransformationWSClient transformationWSClient = new TransformationWSClient(TEST_END_POINT);
        TransformedXMLStringWithDFDLReference xmlResponse = transformationWSClient.transformDataGivenReferenceToDfdlSchema("rsp580", data);
        Assert.assertTrue("xmlResponse should not be null or empty",
                xmlResponse != null && xmlResponse.getTransformedXmlAsString() != null && !xmlResponse.getTransformedXmlAsString().isEmpty());

        byte[] transformedXml = xmlResponse.getTransformedXml();
        Assert.assertArrayEquals("Transformed data must be equals to expected", xml, transformedXml);

    }

    private byte[] getFileContent(String filePath) throws IOException {
        return Files.readAllBytes(Paths.get(filePath));
    }

}
