package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.DFDLSchemaEntry;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.ObjectTypeEntry;
import net.ihe.gazelle.transformation.service.TransformationException;
import net.ihe.gazelle.transformation.service.TransformationWS;
import net.ihe.gazelle.transformation.service.TransformedDataWithDFDLReference;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class TransformationWSClient {

    private static final String NAMESPACE_URI = "http://ws.transformation.gazelle.ihe.net/";
    private static final String SERVICE_NAME = "GazelleTransformationService";

    private TransformationWS transformationWS;

    public TransformationWSClient(String wsdlEndpoint) throws MalformedURLException {
        URL wsdlURL = new URL(wsdlEndpoint);
        QName qname = new QName(NAMESPACE_URI, SERVICE_NAME);
        Service service = Service.create(wsdlURL, qname);
        transformationWS = service.getPort(TransformationWS.class);
    }

    public byte[] transformXMLGivenDfdlSchema(byte[] dfdlSchema, String xmlToTransform)
            throws CompilationException, TransformationException, IllegalArgumentException {
        return transformationWS.transformXMLGivenDfdlSchema(dfdlSchema, xmlToTransform.getBytes(StandardCharsets.UTF_8));
    }

    public TransformedDataWithDFDLReference transformXMLGivenReferenceToDfdlSchema(String dfdlSchemaKeyword, String xmlToTransform)
            throws IllegalArgumentException, ObjectNotFoundException, CompilationException, TransformationException {
        return transformationWS.transformXMLGivenReferenceToDfdlSchema(dfdlSchemaKeyword, xmlToTransform.getBytes(StandardCharsets.UTF_8));
    }

    public String transformDataGivenDfdlSchema(byte[] dfdlSchema, byte[] dataToTransform)
            throws CompilationException, TransformationException, IllegalArgumentException {
        return new String(transformationWS.transformDataGivenDfdlSchema(dfdlSchema, dataToTransform), StandardCharsets.UTF_8);
    }

    public TransformedXMLStringWithDFDLReference transformDataGivenReferenceToDfdlSchema(String dfdlSchemaKeyword, byte[] dataToTransform)
            throws IllegalArgumentException, ObjectNotFoundException, CompilationException, TransformationException {
        return new TransformedXMLStringWithDFDLReference(
                transformationWS.transformDataGivenReferenceToDfdlSchema(dfdlSchemaKeyword, dataToTransform));
    }

    public List<DFDLSchemaEntry> getAvailableDfdlSchemas() throws ObjectNotFoundException {
        return transformationWS.getAvailableDfdlSchemas();
    }

    public List<ObjectTypeEntry> getAvailableObjectTypes() throws ObjectNotFoundException {
        return transformationWS.getAvailableObjectTypes();
    }

    public List<DFDLSchemaEntry> getDfdlSchemasForAGivenObjectType(String objectTypeKeyword) throws ObjectNotFoundException, IllegalArgumentException {
        return transformationWS.getDfdlSchemasForAGivenObjectType(objectTypeKeyword);
    }

    public byte[] getDfdlSchemaContentByKeyword(String dfdlSchemaKeyword) throws ObjectNotFoundException, IOException, IllegalArgumentException {
        return transformationWS.getDfdlSchemaContentByKeyword(dfdlSchemaKeyword);
    }

}
