package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.transformation.service.DFDLSchemaEntry;
import net.ihe.gazelle.transformation.service.TransformedXmlWithDFDLReference;

import java.nio.charset.StandardCharsets;

public class TransformedXMLStringWithDFDLReference extends TransformedXmlWithDFDLReference {

    public TransformedXMLStringWithDFDLReference(TransformedXmlWithDFDLReference transformedXmlWithDFDLReference) {
        super(transformedXmlWithDFDLReference.getTransformedXml(), transformedXmlWithDFDLReference.getDfdlSchema());
    }

    public String getTransformedXmlAsString() {
        return new String(super.getTransformedXml(), StandardCharsets.UTF_8);
    }

}
